{
	"AWSTemplateFormatVersion": "2010-09-09",
	"Description": "Data DB configuration",
	"Parameters": {
		"VPCStack": {
			"Description": "Name of VPC stack",
			"Type": "String",
			"Default": "Test-VPC"
		},
    	"IAMStack": {
      		"Description": "Name of IAM stack",
      		"Type": "String",
      		"Default": "Test-IAM"
    	},
 		"DataDBInitS3LocationBucket": {
			"Description": "Location of Data DB Backup for MyBB (S3 Bucket)",
			"Type": "String",
			"Default": "andreyasoskovtest-files"
		}, 
    	"DataDBInitS3LocationLambdaFile": {
      		"Description": "Location of Data DB Backup for MyBB (S3 key)",
      		"Type": "String",
      		"Default": "datadbinit.lambdapp.zip"
    	}, 
    	"DataDBInitS3LocationInitFile": {
      		"Description": "Location of Data DB Backup for MyBB (S3 key)",
      		"Type": "String",
      		"Default": "backup.sql.zip"
    	},     	 
		"EnvironmentType": {
			"Description": "Type of Environment",
			"Type": "String",
      		"AllowedValues" : ["Prod", "Test"],
      		"Default": "Test",
      		"ConstraintDescription" : "must specify Prod or Test."
		},
		"DBName" : {
			"Description" : "The name of Data DB that should be used for MyBB",
      		"Type": "String",
      		"MinLength" : "1",
    		"MaxLength" : "64",
			"AllowedPattern" : "[a-zA-Z0-9]*",
      		"ConstraintDescription": "Must contain only AlphaNumeric Values (min8-max24)",
      		"Default": "mybbdb"
      	},	
		"DBUser" : {
			"Description" : "The user that should be used in MyBB Data DB to access it",
      		"Type": "String",
      		"MinLength" : "1",
    		"MaxLength" : "16",
			"AllowedPattern" : "[a-zA-Z0-9]*",
      		"ConstraintDescription": "Must contain only AlphaNumeric Values (min8-max24)",
      		"Default": "mybbuser"
      	},		
		"DBPassword" : {
			"Description" : "The password that should be used in MyBB Data DB to access it",
      		"Type": "String",
      		"MinLength" : "8",
    		"MaxLength" : "41",
      		"NoEcho": "true",
			"AllowedPattern" : "[a-zA-Z0-9]*",
      		"ConstraintDescription": "Must contain only AlphaNumeric Values (min8-max24)",
      		"Default": "mybbpassword"
      	}	
	},
	"Mappings": {
	},
	"Resources": {		
		"DataDatabase": {
			"Type": "AWS::RDS::DBInstance",
			"Properties": {
				"AllocatedStorage": "5",
				"MultiAZ" : "false",
				"DBInstanceClass": "db.t2.micro",
				"DBInstanceIdentifier": "db1",
				"DBName": {"Ref": "DBName"},
				"Engine": "MySQL",
				"MasterUsername": {"Ref": "DBUser"},
				"MasterUserPassword": {"Ref": "DBPassword"},
				"PreferredMaintenanceWindow": "Sun:22:00-Sun:23:00",
				"PreferredBackupWindow": "20:00-21:00",
				"BackupRetentionPeriod" : "0",
				"PubliclyAccessible" : "false",
				"VPCSecurityGroups": [{"Fn::ImportValue" : {"Fn::Sub" : "${VPCStack}-DatabaseSecurityGroup"}}],
				"DBSubnetGroupName": {"Ref": "DBSubnetGroup"},
				"Tags": [{ "Key" : "EnvType", "Value" : {"Ref": "EnvironmentType"} }]
			}
		},
		"DBSubnetGroup" : {
			"Type" : "AWS::RDS::DBSubnetGroup",
			"Properties" : {
				"DBSubnetGroupDescription" : "DB subnet group",
				"SubnetIds": [
					{"Fn::ImportValue" : {"Fn::Sub" : "${VPCStack}-PrivateSubnetIDz1"}}, 
					{"Fn::ImportValue" : {"Fn::Sub" : "${VPCStack}-PrivateSubnetIDz2"}}
				],
				"Tags": [{ "Key" : "EnvType", "Value" : {"Ref": "EnvironmentType"} }]
			}
		},
    "DBInit": {
      "Type": "Custom::DBInit",
      "Properties": {
        "ServiceToken": { "Fn::GetAtt" : ["CreateEmptyTables", "Arn"] },
        "S3Bucket": {"Ref": "DataDBInitS3LocationBucket"},
        "S3Key": {"Ref": "DataDBInitS3LocationInitFile"},
        "DBName": {"Ref": "DBName"},
        "DBHost": {"Fn::GetAtt": ["DataDatabase", "Endpoint.Address"]},
        "DBUser": {"Ref": "DBUser"},
        "DBPassword": {"Ref": "DBPassword"}
      }
    },
    "CreateEmptyTables": {
      "Type": "AWS::Lambda::Function",
      "Properties": {
        "Handler": "index.handler",
        "Role": {"Fn::ImportValue" : {"Fn::Sub" : "${IAMStack}-DataDBInitLambdaIAMRoleArn"}},
        "Code": {
          "S3Bucket": { "Ref": "DataDBInitS3LocationBucket" },
          "S3Key": { "Ref": "DataDBInitS3LocationLambdaFile" }
        },        
        "Runtime": "nodejs4.3",
        "Timeout": "60",
          "VpcConfig": {
          	"SecurityGroupIds": [
           		{"Fn::ImportValue" : {"Fn::Sub" : "${VPCStack}-DBProvisioningSecurityGroup"}}                
           		],             
           		"SubnetIds": [
                		{"Fn::ImportValue" : {"Fn::Sub" : "${VPCStack}-ManageSubnetIDz1"}}, 
                		{"Fn::ImportValue" : {"Fn::Sub" : "${VPCStack}-ManageSubnetIDz2"}}                
              		]
            	}
      		}
    },
    "ElasticCacheCluster" : {
      "Type": "AWS::ElastiCache::CacheCluster",
      "Properties": {
        "AZMode"  :  "cross-az",
        "CacheNodeType"           : "cache.t2.micro",
        "CacheSubnetGroupName" : {"Ref": "ElasticCacheSubnetGroup"},
        "VpcSecurityGroupIds" : [ {"Fn::ImportValue" : {"Fn::Sub" : "${VPCStack}-ElacticCacheSecurityGroup"}} ],
        "Engine"                  : "memcached",
        "NumCacheNodes"           : "1",
        "PreferredMaintenanceWindow" : "Sun:22:00-Sun:23:00",
        "Tags": [{ "Key" : "EnvType", "Value" : {"Ref": "EnvironmentType"} }]
      }
    },
    "ElasticCacheSubnetGroup" : {
      "Type" : "AWS::ElastiCache::SubnetGroup",
      "Properties" : {
        "Description" : "Subnet Group for Elastic Cache for Data DB",
        "SubnetIds": [
          {"Fn::ImportValue" : {"Fn::Sub" : "${VPCStack}-PrivateSubnetIDz1"}}, 
          {"Fn::ImportValue" : {"Fn::Sub" : "${VPCStack}-PrivateSubnetIDz2"}}
        ]
      }
    }
	},
	"Outputs": {
		"DataDBEndPointAddress": {
			"Value": {"Fn::GetAtt": ["DataDatabase", "Endpoint.Address"]},
			"Description": "MyBB Data DB EndPoint Address",
      "Export" : { "Name" : {"Fn::Sub": "${AWS::StackName}-DBEndPointAddress" }}
		},
    "ElasticClusterEndPointAddress": {
      "Value": {"Fn::GetAtt": ["ElasticCacheCluster", "ConfigurationEndpoint.Address"]},
      "Description": "MyBB Elastic Cache EndPoint Address",
      "Export" : { "Name" : {"Fn::Sub": "${AWS::StackName}-ElasticClusterEndPointAddress" }}
    }
	}
}