var response = require('cfn-response');
var AWS = require('aws-sdk');
//var zlib = require('zlib');
var AdmZip = require('adm-zip');
var mysql = require('mysql');
var s3 = new AWS.S3(); 

exports.handler = function(event, context) {
      console.log('REQUEST RECEIVED:\n');  
      console.log('Event:\n', JSON.stringify(event));
      console.log('Context:\n', JSON.stringify(context));

      if (event.RequestType != 'Create') {
            response.send(event, context, response.SUCCESS);
            return;
      }

      var s3params = {
            Bucket: event.ResourceProperties.S3Bucket,
            Key: event.ResourceProperties.S3Key
      };
      var myCon  = mysql.createConnection({
            host     : event.ResourceProperties.DBHost,
            database : event.ResourceProperties.DBName,
            user     : event.ResourceProperties.DBUser,
            password : event.ResourceProperties.DBPassword,
            connectTimeout: 20000,
            multipleStatements: true
      });
      console.log('s3params:\n', JSON.stringify(s3params));

      s3.getObject(s3params, function(err,data) {

            if (err) {
                  console.log("Error reading SQL from S3:", JSON.stringify(err, null, 2));                              
                  response.send(event, context, response.FAILED);
                  return;  
                  }
            console.log("S3 is read succesfully!"); 
 
//            console.log("ZIPed content: ", JSON.stringify(data.Body, null, 2));  

            var zip = new AdmZip(data.Body);
            var zipEntries = zip.getEntries();
            console.log("ZIP_entries: ", JSON.stringify(zipEntries, null, 2));  
            var SQLstatement = zip.readAsText(zipEntries[0],'ascii');

//            console.log("UNZIPed content: ", JSON.stringify(SQLbuffer, null, 2));  

//            SQLstatement = "CREATE TABLE `mybb_adminlog` (`uid` int(10) unsigned NOT NULL DEFAULT '0', KEY `uid` (`uid`)) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
//            var SQLstatement = data.Body.toString('ascii'); 
             
//            console.log("SQL to execute: ", SQLstatement);              
            
            myCon.query(SQLstatement, function(err, results) {   
                  if (err) {
                        myCon.end();                      
                        console.log("Error in SQL Execution: ", JSON.stringify(err, null, 2))                    
                        response.send(event, context, response.FAILED);
                        return;  
                        }
                  myCon.end();
                  console.log("SQL is executed successfully!");                            
                  response.send(event, context, response.SUCCESS);
          });
      });
};