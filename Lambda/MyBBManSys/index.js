//var http = require('http');
//var port = process.env.PORT || 3000;
//var server = http.createServer(SendResult).listen(port);
var AWS = require("aws-sdk");
var elasticbeanstalkMyBB = new AWS.ElasticBeanstalk();
var EnvName = "MyBBAppEnv";
var EBparams = {
  EnvironmentName: EnvName,
  AttributeNames: [ "All" ] 
};

exports.handler = function(event, context) {
  console.log('REQUEST RECEIVED:\n');  
  console.log('Event:\n', JSON.stringify(event));
  console.log('Context:\n', JSON.stringify(context));

  elasticbeanstalkMyBB.describeInstancesHealth(EBparams, function(err, data) {
    if (err) {
      console.error("Unable to get Beanstalk Environment Health. Error JSON:", JSON.stringify(err, null, 2));                              
      context.fail(err);
    }
    else {
      console.log("Beanstalk Environment Health succesfully received:", JSON.stringify(data, null, 2));                             

      var body = '<html><head><meta charset="utf-8"><title>MyBB Statistics</title></head>' +
                 '<body>' +
                 '<table border="1"> <tbody> MyBB Statistics' +
                 '<tr> <td>InstanceID</td><td>CPU_User</td><td>CPU_Sys;</td><td>CPU_Idle</td><td>CPU_IO</td></tr>' +
                 '<tr>' + 
                 '<td>' + data.InstanceHealthList[0].InstanceId + '</td><td>' + data.InstanceHealthList[0].System.CPUUtilization.User +'</td>' +
                 '<td>' + data.InstanceHealthList[0].System.CPUUtilization.System + '</td><td>' + data.InstanceHealthList[0].System.CPUUtilization.Idle  + '</td>' +
                 '<td>' + data.InstanceHealthList[0].System.CPUUtilization.IOWait + '</td>' +
                 '</tr>' +
                 '</tbody></table>' +
                 '</body></html>';  

      console.log('HTML:\n', body;
      context.succeed(body);
    }
  });
};  